
-- SUMMARY --
The Field Group Next Pre module will provide the additional next pre button with field group tabs in node form.


Installation
------------

Copy coder.module to your module directory and then enable on the admin
modules page.  Enable the modules that admin/config/development/coder/settings
works on,then create the fieldgroup horizontal/vertical tabs with next pre.

Author
------
Hemant Sharma
hemant.sharma@sdgc.com


